Cosmic Tranquility *(WIP)*
======

[![ContentDB](https://content.minetest.net/packages/s20/cosmic_tranquility/shields/downloads/)](https://content.minetest.net/packages/s20/cosmic_tranquility/)

![screenshot.png](screenshot.png)

A [Minetest](https://https://github.com/minetest/minetest/) [Texture Pack](https://content.minetest.net/packages/?type=txp) bringing out the colors of the cosmos and some quirky modifications (including [Capture the Flag](https://github.com/MT-CTF/capturetheflag/) changes)!

This texturepack also has support for the [keystrokes CSM](https://github.com/Minetest-j45/keystrokes). The colour scheme for the key textures is based on the [Mocha Catppuccin Pallete](https://github.com/catppuccin/catppuccin#-palette).

Table of Contents
-----

[[_TOC_]]

Installation
------

> *You can also install the texture pack from the ContentDB integrated in your Mintest client.*

### Fetching the Texture Pack Folder
#### Using Git

> Make sure you have [Git](https://git-scm.com/) installed!

```bash
cd [YOUR MINTEST FOLDER]
cd textures && git clone https://gitlab.com/src4026/cosmic_tranquility.git
```

#### Without using Git

1. Download the source code from the methods provided by GitLab for this repository or from [ContentDB](https://content.minetest.net/packages/s20/cosmic_tranquility/).
2. Extract the `.zip`/`tar.gz`/`tar.bz2`/`tar` and move it to `[YOUR MINETEST FOLDER]/textures`.

### Enabling the Texture Pack

1. Open your Mintest Client and navigate to the `Content` tab.
2. Select the texture and enable it/Double click to enable or disable.

> You can also edit `[YOUR MINETEST FOLDER]/minetest.conf` to set the `texture_path` variable to the path of the texture.
> ```
> texture_path = [YOUR MINETEST FOLDER]/textures/cosmic_tranquility
> ```

TODO
-----

- [x] Swords
- [ ] Other tools
- [ ] Check if the gun textures need modification. If not, leave as it is.
- [ ] Check how player textures work.
- [ ] Find the overall "cosmic theme"
- [ ] GUI changes
- [ ] Block textures

License
------
Refer [LICENSE.md](LICENSE.md) for detailed information.
